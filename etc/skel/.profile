# ~/.profile: executed by the command interpreter for login shells.
# All other interactive shells will only read .bashrc; 
# Login shell

test -z "$PROFILEREAD" && . /etc/profile || true

# this is particularly important for language settings, see below.
#export LANG=de_DE.UTF-8        # uncomment this line for German output
#export LANG=fr_FR.UTF-8        # uncomment this line for French output
#export LANG=es_ES.UTF-8        # uncomment this line for Spanish output

# set PATH so it includes user's private bin if it exists
test -d ~/bin && PATH="~/bin:$PATH" || true

. ~/.etc/profile.d/myrc.functions

if [ x"$DIST" = x"suse" ]; then
  export INPUT_METHOD=fcitx
fi

# -- deb -- #
[ -n "$DEBEMAIL" ] && export DEBEMAIL DEBFULLNAME
# -- end deb -- #

# -- ruby -- #
[ -n "$GEM_PATH" ] && export GEM_PATH
[ -n "$GEM_HOME" ] && { export GEM_HOME; PATH=$GEM_HOME/bin:$PATH; }
# -- end ruby -- #

# -- java -- #
[ -d $JAVA_HOME ] && { export JAVA_HOME=$JAVA_HOME; PATH=$JAVA_HOME/bin:$PATH; }
# -- end java -- #

# -- qt -- #
[ -d $QTDIR ] && { export QTDIR=$(get_realpath $QTDIR); PATH=$(get_realpath $QTDIR)/qt/bin:$PATH; LD_LIBRARY_PATH=$QTDIR/qt/lib:$LD_LIBRARY_PATH; export LD_LIBRARY_PATH; }
# -- end qt -- #

# -- context -- #
[ -n $OSFONTDIR ] && export OSFONTDIR
[ -s $CTXDIR/tex/setuptex ] && sh $CTXDIR/tex/setuptex $CTXDIR/tex >/dev/null 2>&1
# -- end context -- #

[ -d ~/.local/bin ] && PATH=~/.local/bin:$PATH
export PATH

if [ ! -f ~/.myrc.touched ];then
  myinit
fi

. ~/.shrc
unset myinit init_ssh init_font init_dropbox init_ruby init_fcitxrc
unset get_realpath set_font set_dbus
#.profile end here
