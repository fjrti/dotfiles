# general
alias ..='cd ..'
alias cd..="cd .."
alias l='ls -CF'
alias ll='ls -lh'
alias la='ls -la'
alias rm='rm -i'
alias rd=rmdir
alias md='mkdir -p'
alias mv='mv -i'
alias cp='cp -i'
alias df='df -h'
alias du='du -h'
alias grep='grep --color=auto'
alias xsudo='sudo env PATH=$PATH'
alias now='date +%Y%m%d%H%M'
alias psD="ps -eal | awk '{ if (\$2 == \"Z\") {print $4}}'"
alias screen='screen -O -t `hostname`'
alias keyon='ssh-add -t 10800'
alias keyoff='ssh-add -D'
alias keylist='ssh-add -l'
alias cman='man -S 2:3:1'
alias sssh='ssh -4 -C -c blowfish-cbc'
alias scp_resume="rsync --compress-level=3 --partial --progress --rsh=ssh"
alias :q='tput setaf 1; echo >&2 "this is NOT vi(m) :/"; tput sgr0'

# shortcut
alias c="clear"
alias g='git'
alias h='history'
alias j="jobs -l"

# enable color support of ls and also add handy aliases
if [ "$TERM" != "dumb" ]; then
    alias ls='ls --color=auto'
    alias dir='ls --color=auto --format=vertical'
    alias vdir='ls --color=auto --format=long'   
fi

alias reloadrc='. ~/.myrc'
alias fixssh='. ~/.ssh/sshenv'
alias fixdisplay='. ~/.local-display-coordinates.sh'
alias sshagent='sshagent.sh; . ~/.ssh/sshenv'
alias texclean='rm -f *.toc *.aux *.log *.cp *.fn *.tp *.vr *.pg *.ky'
alias clean='echo -n "Really clean this directory?";
        read yorn;
        if test "$yorn" = "y"; then
           rm -f \#* *~ .*~ *.bak .*.bak  *.tmp .*.tmp core a.out;
           echo "Cleaned.";
        else
           echo "Not cleaned.";
        fi'
