#!/bin/bash

START_DIR=`pwd`

# get absolute path to the top level directory
cd `dirname $0`
ROOT_DIR=`pwd`

trap 'echo "install failed!' 1
trap 'echo "install interrupted!"' 2 3 6 15

. etc/profile.d/bash_colors.sh
[ -d etc/ ] || { echo ${RED}etc/${NONE} not found; exit; }
[ "$(command -v rsync)" ] || { echo ${RED}rsync${NONE} not found; exit; }
rsync -arz etc/ ~/.etc/

#######################################
# link dot files
#

echo "linking dot files..."
# find all '.' files that don't end in .swp and is not .git/
for file in `find etc/skel -name "*.sh" \
        -o -path "*.git/*" \
        -o -path "etc/skel/.tmux/*" \
        -o -name ".*.swp" \
        -o -name ".git" \
        -prune -o -type f -print`; do
    sourcefile=$file
    destfile=`echo $file | grep etc/skel/ | sed -e 's#etc/skel/##'`
    if [ ! -e ~/$destfile ]; then
        echo "  ln -s ~/.$sourcefile ~/$destfile"
        [ -d $(dirname ~/$destfile) ] || mkdir -p $(dirname ~/$destfile)
        ln -s ~/.$sourcefile ~/$destfile
    else
        echo "  $destfile already exists"
    fi
done
echo ""

[ ! -L ~/.bashrc ] && ln -sf ~/.etc/skel/.shrc ~/.bashrc
[ ! -L ~/.tmux ] && ln -sf ~/.etc/skel/.tmux ~/.tmux
#
#######################################

. etc/profile.d/myrc.functions 
myinit

echo "install complete!"
echo ""
